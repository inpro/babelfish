import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import babelfish.Translator;


public class TranslatorTest {

	@SuppressWarnings("unused")
	@Test
	public void testCreation() {
		new Translator();
	}
	
	@SuppressWarnings("unused")
	@Test
	public void testCreationFromStream() {
		try {
			new Translator(Translator.class.getResourceAsStream("translationtable"));
		} catch (IOException e) {
			fail(e.toString());
		}
	}
	
	@Test
	public void testTranslate() {
		Translator tr = new Translator();
		tr.translate("eins");
		tr.translate("fünf");
	}
	
	@Test(expected=AssertionError.class)
	public void testTranslateFails() {
		Translator tr = new Translator();
		tr.translate("somethingthatISUNLIKELYtobetranslateable");
	}

}
