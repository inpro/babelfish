package synthesis;

import org.junit.Test;

public class SynthesisRunnerTest {

	@Test
	public void test() {
		// setup
		SynthesisRunner synRunner = new SynthesisRunner("Dies", "ist", "ein", "langer", "komplizierter Satz");
		synRunner.playNextPhrase(); synRunner.playNextPhrase();
		synRunner.dispatcher.waitUntilDone();
	}

}
