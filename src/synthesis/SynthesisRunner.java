package synthesis;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import inpro.apps.SimpleMonitor;
import inpro.audio.DispatchStream;
import inpro.incremental.IUModule;
import inpro.incremental.processor.SynthesisModule;
import inpro.incremental.unit.ChunkIU;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.IU.IUUpdateListener;

public class SynthesisRunner extends IUModule implements IUUpdateListener {
	
	/** audio stream that we dispatch to */
	DispatchStream dispatcher;
	
	List<ChunkIU> phrases;
	
	/** set up the synthesis runner */
	public SynthesisRunner() {
		// get an output object that plays back on the speakers/headphone
		dispatcher = SimpleMonitor.setupDispatcher();
		// setup the synthesis module to play audio to this dispatcher
		SynthesisModule synth = new SynthesisModule(dispatcher);
		this.addListener(synth);
	}
	
	public SynthesisRunner(String ... words) {
		this();
		phrases = new ArrayList<ChunkIU>();
		for (String word : words) {
			ChunkIU phrase = new ChunkIU(word);
			phrase.addUpdateListener(this);
			phrases.add(phrase);
		}
	}
	
	public void playNextPhrase() {
		if (phrases.size() > 0) {
			ChunkIU phrase = phrases.get(0);
			phrases.remove(0);
			rightBuffer.addToBuffer(phrase);
			notifyListeners();
		}
	}
	
	@Override
	public void update(IU updatedIU) {
		if (updatedIU.isCompleted()) {
			playNextPhrase();
		}
	}
	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius,
			List<? extends EditMessage<? extends IU>> edits) {
		// nothing to do here, we do not plan to consume IUs but only to produce IUs for the time being.
	}

}
