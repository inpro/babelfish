package babelfish;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import inpro.incremental.IUModule;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.HesitationIU;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.WordIU;

/** 
 * a simple IUModule that 
 * @author timo
 */
public class TranslationModule extends IUModule {

	Translator tr = new Translator();
	
	@SuppressWarnings({ "rawtypes", "unchecked" }) 
	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius,
			List<? extends EditMessage<? extends IU>> edits) {
		for (EditMessage edit : edits) {
			logger.warn(edit.getType() + " " + edit.getIU().toPayLoad());
			WordIU inputWord = (WordIU) edit.getIU();
			// skip silences:
			if (inputWord.isSilence()) 
				continue;
			switch (edit.getType()) {
			case ADD: 
				String originalToken = inputWord.toPayLoad();
				// don't do anything for silence
				String translatedToken = tr.translate(originalToken);
				WordIU outputWord;
				if (translatedToken != null) {
					outputWord = new WordIU(translatedToken, null, Collections.<IU>singletonList(inputWord));
				} else {
					outputWord = new HesitationIU();
				}
				rightBuffer.addToBufferSetSLL(outputWord);
				break;
			case REVOKE:
				EditMessage outEdit = new EditMessage(EditType.REVOKE, inputWord.grounds().get(0));
				rightBuffer.editBuffer(outEdit);
				break;
			case COMMIT: 
				rightBuffer.clearBuffer();
				break;
			}
		}
	}

}
