package babelfish;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Translator {
	
	final Map<String, String> translationTable = new HashMap<String, String>();

	public Translator() {
		try {
			loadTranslationTable(Translator.class.getResourceAsStream("translationtable"));
		} catch(IOException ioe) {
			throw new RuntimeException("problem reading resource", ioe);
		}
	}

	public Translator(InputStream source) throws IOException {
		loadTranslationTable(source);
	}
	
	private void loadTranslationTable(InputStream source) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(source));
		String line = br.readLine();
		while (line != null) {
			String[] tokens = line.split("\\\t");
			if (tokens.length != 2) {
				throw new RuntimeException("problem parsing line " + line);
			}
			translationTable.put(tokens[0], tokens[1]);
			line = br.readLine();
		}
	}
	
	public String translate(String key) {
		String translation = translationTable.get(key);
		return translation;
	}
	
}
