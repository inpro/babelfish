package babelfish;

import java.util.Collection;
import java.util.List;

import inpro.incremental.IUModule;
import inpro.incremental.unit.ChunkIU;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.WordIU;

/** 
 * a simple IUModule that 
 * @author timo
 */
public class PartialTranslationModule extends IUModule {

	Translator tr = new Translator();
	
	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius,
			List<? extends EditMessage<? extends IU>> edits) {
		// TODO 1: deal with incoming IUs, or deal with incoming EditMessages
		// (there may be multiple edit messages in one call, 
		// signalling that multiple IUs have changed since the last call)
		// TODO 2: generate some output and pass it on to rightBuffer using e.g.:
		// rightBuffer.addToBuffer(IU), or rightBuffer.editBuffer(EditMessage).
		// TODO 3: that's it, the next module will be called automatically.
		
		for (EditMessage e : edits) {
			IU incomingWord = (WordIU) e.getIU();
			switch (e.getType()) {
			case ADD :
				String t = tr.translate(incomingWord.toPayLoad());
				ChunkIU translatedPhrase = new ChunkIU(t);
				incomingWord.groundIn(translatedPhrase);
				rightBuffer.addToBuffer(translatedPhrase);
				break;
			case REVOKE :
				if (incomingWord.isUpcoming()) {
					rightBuffer.editBuffer(new EditMessage(EditType.REVOKE, incomingWord.groundedIn().get(0)));
				} else {
					rightBuffer.addToBuffer(new ChunkIU("sorry"));
				}
				break;
			}
		}
		
	}
}


/**/